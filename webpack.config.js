
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const autoprefixer = require('autoprefixer');


module.exports = {
    entry: './src/app.js',
    output: {
        path: __dirname + "/dist/",
        filename: "bundle.js",
        chunkFilename: '[name].js'
    },
    watchOptions: {
        poll: 1000 // Check for changes every second
    },
    devServer: {
        contentBase: __dirname,
        hot: true,
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    autoprefixer({
                                        browsers:['ie >= 8', 'last 4 version']
                                    })
                                ],
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader'
                        },
                    ],
                })
            },
            {
                test:/\.pug$/,
                loader: 'pug-loader',
                options: {
                    pretty: true
                }
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000',
                    'img-loader'
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader?name=./font/Lora/[name].[ext]'
                    },
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: true, // webpack@1.x
                            disable: true, // webpack@2.x and newer
                        },
                    },
                ],
            },
        ]
    },

  plugins: [
    new HtmlWebpackPlugin({
      template: __dirname + '/src/pug/index.html',
      filename: __dirname + '/dist/index.html'
    }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/about.pug',
          filename: __dirname + '/dist/about.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/catalog.pug',
          filename: __dirname + '/dist/catalog.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/order.pug',
          filename: __dirname + '/dist/order.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/contacts.pug',
          filename: __dirname + '/dist/contacts.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/catalog-detail.pug',
          filename: __dirname + '/dist/catalog-detail.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/sklad.pug',
          filename: __dirname + '/dist/sklad.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/favorite.pug',
          filename: __dirname + '/dist/favorite.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/project.pug',
          filename: __dirname + '/dist/project.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/reviews.pug',
          filename: __dirname + '/dist/reviews.html'
      }),
    new ExtractTextPlugin('[name].css'),
    new BrowserSyncPlugin({
      // browse to http://localhost:3000/ during development,
      // ./public directory is being served
      host: 'localhost',
      port: 8080,
      server: { baseDir: __dirname + "/dist/" },
    })
    ],
    optimization: {
        minimizer: [
          new TerserPlugin(),
          new OptimizeCSSAssetsPlugin({})
        ]
    }
};
