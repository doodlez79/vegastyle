'use strict';

const $ = require("jquery");

window.jQuery = $;

if ($) {
    require('bootstrap');
    require('slick-carousel/slick/slick.min');
    require('@fancyapps/fancybox/dist/jquery.fancybox.min');
    require('slideout/dist/slideout.min')
    require("./js/main.js");
    require("./scss/required.scss");
}
