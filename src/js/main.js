import Slideout from "slideout";

// //preloader SAMVEL
// let ss = document.querySelectorAll("#Gradient2 stop ");
// let sss = document.querySelectorAll("#Gradient3 stop");
// let oneSvg = document.querySelector("#oneSvg");
// let start = null;
// let progress = 0;//the progress of the animation
// let duration = 2000; //2 seconds in millisecinds
// let rid = null;//request animation id
//
// function Frame(timestamp) {
//     rid = window.requestAnimationFrame(Frame);
//
//     if (!start) start = timestamp;
//     progress = timestamp - start;
//
//     if (progress <= duration) {
//         //for each stop reset the offset attribute
//         ss.forEach(s => {
//             s.setAttributeNS(null, "offset", 1- progress / duration);
//         });
//         sss.forEach(s => {
//             s.setAttributeNS(null, "offset", 1- progress / duration);
//         });
//     }
//     // if the animation is over cancel the animation
//     if (progress >= duration){
//         oneSvg.style.stroke = "red"
//         jQuery("#test2").addClass("custom-animation")
//         jQuery("#word").addClass("custom-animation-fade-in")
//         window.cancelAnimationFrame(rid)
//     }
// }
//
// window.onload = function () {
//     Frame(null)
//     // console.log((new Date).getTime() - window.startTime);
// }
// window.addEventListener('load',function () {
//     // const blackBg = jQuery("#preloaderBlack")
//     // console.log(blackBg, "blackBg", whiteBg)
//     // jQuery("#preloaderWhite").stop().animate({'offset': 100}, 1000, 'easeOutBounce')
//     // jQuery("#preloaderBlack").stop().animate({'offset': 100}, 1000, 'easeOutBounce')
//
// },false)

jQuery(document).ready(function() {
    jQuery('#scrollDiv').hide();
    jQuery(window).scroll(function() {
        if (jQuery(document).scrollTop() > 100) {
            jQuery(".header_wrapper").addClass("shadow-header")
        }
        else {
            jQuery(".header_wrapper").removeClass("shadow-header")
        }
    });
    const slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        "touch": false,
        'padding': 256,
        'tolerance': 70,
        'side': "right"
    });

    document.querySelector('.toggle-button').addEventListener('click', function() {
        slideout.toggle();
    });
    document.querySelector('.mobile-menu-close').addEventListener('click', function() {
        slideout.close();
    });
    jQuery('.about-carousel').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });

    let catalogItems = jQuery(".catalog-item-js")

    catalogItems.each(function (){

        let mainBg = this.querySelector(".catalog-item-img").style.backgroundImage
        this.addEventListener("mouseenter", () => HoverCatalogItem(this, mainBg) )

    })


    jQuery(".js-header-call").click(function() {
        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery(".magnet").offset().top - 150
        }, 1000);
    });


    function HoverCatalogItem (elem, oldImg) {


        const currentElem = jQuery(elem).find(".catalog-item-img")

        elem.addEventListener("mouseleave", function () {
            currentElem.css("backgroundImage", oldImg);
        })



        const Elems = currentElem.find(".js-hover-slider").find(".js-hover-slider-item")
        Elems.each(function () {
            let imgPath = this.dataset.img
            this.addEventListener("mouseenter", function (){
                currentElem.css("backgroundImage", `url('${imgPath}')`);
            })
        })
    }

    jQuery(document).ready(function(){
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 0) {
                jQuery('#scroller').fadeIn();
            } else {
                jQuery('#scroller').fadeOut();
            }
        });
        jQuery('#scroller').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });

    if (jQuery("#map").has().length) {
        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [55.751574, 37.573856],
                    zoom: 9
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">jQuery[properties.iconContent]</div>'
                ),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: 'Собственный значок метки',
                    balloonContent: 'Это красивая метка'
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'images/myIcon.gif',
                    // Размеры метки.
                    iconImageSize: [30, 42],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-5, -38]
                }),

                myPlacemarkWithContent = new ymaps.Placemark([55.661574, 37.573856], {
                    hintContent: 'Собственный значок метки с контентом',
                    balloonContent: 'А эта — новогодняя',
                    iconContent: '12'
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#imageWithContent',
                    // Своё изображение иконки метки.
                    iconImageHref: 'images/ball.png',
                    // Размеры метки.
                    iconImageSize: [48, 48],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-24, -24],
                    // Смещение слоя с содержимым относительно слоя с картинкой.
                    iconContentOffset: [15, 15],
                    // Макет содержимого.
                    iconContentLayout: MyIconContentLayout
                });

            myMap.geoObjects
                .add(myPlacemark)
                .add(myPlacemarkWithContent);
        });
    }




    jQuery('[data-fancybox="gallery"]').fancybox({
        // Options will go here
    });
});
